#!/bin/bash
# SET PATH WHERE THE IMAGES ARE LOCATED
FILES_PATH=''
# SET GROUPS OF IMAGES
GROUP=450
# PREFIX FROM OUTPUT FOLDERS
DIRS_NAME='DIR_'
COUNTER=1
NEXT=0
for i in $(ls $PWD/$FILES_PATH -I *.sh); do
    if (($COUNTER == $NEXT || $COUNTER == 1)); then
        echo "CREATE FOLDER $DIRS_NAME$COUNTER"
        $(mkdir $PWD$FILES_PATH/$DIRS_NAME$COUNTER)
        DIR_DEST="$PWD$FILES_PATH/$DIRS_NAME$COUNTER/"
        $(mv $PWD$FILES_PATH/$i $DIR_DEST)
        NEXT=$[$NEXT +$GROUP]
    else
        $(mv $PWD$FILES_PATH/$i $DIR_DEST)
    fi
    COUNTER=$[$COUNTER +1]
done
echo "====================================="
echo "================ END ================"
echo "====================================="