#!/bin/bash
# ORIGINAL AND OPTIMIZED IMAGE PATHS
FILES_PATH='/originales'
FILES_OP_PATH='/optimizadas'

# SET YOUR APIKEY FROM TINYPNG  https://tinypng.com/developers
KEY_API=''

#SETUP COLORS FROM OUTPUT BASH
CYAN='\033[0;34m'
YELLOW='\033[0;32m'
YELLOW='\033[0;32m'
GRAY='\033[0;37m'
NC='\033[0m' # No Color

COUNTER=0
for i in $(ls $PWD/$FILES_PATH); do
    COUNTER=$[$COUNTER +1]
    echo "==============================="
    echo "============== $COUNTER ============="
    echo "==============================="
    $(curl https://api.tinify.com/shrink --user api:$KEY_API --data-binary @$PWD$FILES_PATH/$i > up.json)
    UPLOAD_SIZE=$(cat up.json | jq -r '.input.size')
    DOWNLOAD_SIZE=$(cat up.json | jq -r '.output.size')
    URL_DOWNLOAD=$(cat up.json | jq -r '.output.url')
    $(curl $URL_DOWNLOAD --user api:$KEY_API --output $PWD$FILES_OP_PATH/$i)
    INFO_BASH="FILE: ${GRAY}$i${NC} | ORIGINAL SIZE: ${CYAN}$UPLOAD_SIZE${NC} => OPTIMIZED SIZE: ${YELLOW}$DOWNLOAD_SIZE${NC}"
    INFO_FILE="FILE: $i | ORIGINAL SIZE: $UPLOAD_SIZE => OPTIMIZED SIZE: $DOWNLOAD_SIZE"
    echo $INFO_BASH
    echo $INFO_FILE >> REPORT.txt
done
echo "==============================="
echo "============== END ============"
echo "==============================="
